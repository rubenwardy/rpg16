BEGIN {
    header_count = 0;
    long_description = "";
}

/^#/ {
    header_count++;
}

{
    if (header_count > 1 && header_count < 6) {
        if (long_description == "") {
            long_description = $0;
        } else {
            long_description = long_description "\\n" $0;
        }
    }
}

END {
    print long_description;
}
